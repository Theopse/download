defmodule Download do
  @moduledoc """
  Documentation for `Download`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Download.hello()
      :world

  """
  def hello do
    :world
  end

  def from(url, otps \\ []) do
    __MODULE__.Server.async(url, otps)
    |> __MODULE__.Server.await(:infinity)
  end


end
