# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
# This File if From Theopse (Self@theopse.org)
# Licensed under BSD-3-Caluse
# File:	bvid.ex (download/lib/server.ex)
# Content:
# Copyright (c) 2021 Theopse Organization All rights reserved
# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

defmodule Download.HTTP do
  import Standard

  begin "Function Defination" do
    def ssl, do: [ssl: [{:versions, [:"tlsv1.2", :"tlsv1.1", :tlsv1]}]]

    def headers do
      [
        "User-Agent": user_agent()
      ]
    end

    def headers(otps) do
      Enum.reduce(otps, ["User-Agent": user_agent()], fn otp, headers ->
        case otp do
          {:userAgent, false} ->
            List.keydelete(headers, :"User-Agent", 0)

          {:userAgent, item} ->
            List.keyreplace(headers, :"User-Agent", 0, {:"User-Agent", item})

          {:"user-agent", false} ->
            List.keydelete(headers, :"User-Agent", 0)

          {:"user-agent", item} ->
            List.keyreplace(headers, :"User-Agent", 0, {:"User-Agent", item})

          {:referer, item} ->
            [{:Referer, item} | headers]

          {:cookies, list} when list?(list) ->
            headers

          {:cookies, item} ->
            [{:Cookie, item} | headers]

          {:host, item} ->
            [{:Host, item} | headers]

          _ ->
            headers
        end
      end)
    end

    @spec user_agent :: binary
    def user_agent do
      [
        "Mozilla/5.0 (X11; FreeBSD amd64; rv:81.0) Gecko/20100101 Firefox/99.0",
        "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:81.0) Gecko/20100101 Firefox/99.0",
        "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:81.0) Gecko/20100101 Firefox/99.0",
        "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/99.0",
        "Mozilla/5.0 (X11; FreeBSD arm64; rv:81.0) Gecko/20100101 Firefox/99.0",
        "Mozilla/5.0 (X11; OpenBSD amd64; rv:81.0) Gecko/20100101 Firefox/99.0"
      ]
      |> Enum.random()
    end
  end
end
