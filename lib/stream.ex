# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
# This File if From Theopse (Self@theopse.org)
# Licensed under BSD-3-Caluse
# File:	stream.ex (download/lib/stream.ex)
# Content:
# Copyright (c) 2021 Theopse Organization All rights reserved
# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

defmodule Download.Stream do
  defstruct url: nil, start: nil, content: nil, error: nil, final: nil

	def new(url \\ nil, start \\ nil, content \\ nil, error \\ nil, fianl \\ nil)
	def new(url, start, content, error, final) do
		start = start || fn _ -> nil end
		content = content || fn _, _ -> nil end
		error = error || fn _, item -> IO.inspect(item) end
		final = final || fn -> nil end

		%Download.Stream{url: url, start: start, content: content, error: error, final: final}
	end
end
